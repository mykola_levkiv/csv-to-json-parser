const fs = require('fs-extra'),
  path = require('path'),
  extract = require('extract-zip'),
  uuid = require('uuid'),
  csvStream = require('csv-stream-to-json'),
  filepath = path.join('json', '/' + uuid.v4() + '.json'),
  tmpFolder = path.join(__dirname + '/tmp'),
  passedPath = process.argv[2];
Promise = require('bluebird');

if (!passedPath.length) {
  console.error('Please provide application with path to zip file csv files in which should be transformed to json');
} else if (!fs.existsSync('json')) {
  fs.mkdirSync('json');
}

const extractPromise = Promise.promisify(extract),
  writeFilePromise = Promise.promisify(fs.writeFile),
  finalJson = [];

extractPromise(passedPath, {dir: tmpFolder})
  .then(() => {
    const csvFilepathArr = fs.readdirSync(tmpFolder);
    return Promise.all(csvFilepathArr.map(filepath => {
      return new Promise((resolve, reject) => {
        const readStream = fs.createReadStream(path.join(tmpFolder, filepath));
        csvStream.parse(readStream, "||", false, json => {
          finalJson.push({
            name: trimQuotes(json['"name"']),
            phone: trimQuotes(json['"phone"'].replace(/[^\d]/g, '')),
            date: parseDateFormat(json['"date"']),
            costCenterNum: trimQuotes(json['"cc"'].replace(/[^\d]/g, ''))
          })
        }, resolve);
      });
    }));
  })
  .then(() => {
    return writeFilePromise(filepath, JSON.stringify(finalJson, null, 2));
  })
  .then(() => {
    console.log('JSON file was successfully created. Please check ' + filepath + ' from project root');
    fs.removeSync(tmpFolder);
  })
  .catch(console.error)

function parseDateFormat(dateStr) {
  const dateArr = dateStr.split('/').map(elem => trimQuotes(elem));
  const dateObj = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]);
  let month = dateObj.getMonth() + 1,
    date = dateObj.getDate();
  month = month < 10 ? '0' + month : month;
  date = date < 10 ? '0' + date : date;

  return `${dateObj.getFullYear()}-${month}-${date}`;
}

function trimQuotes(str) {
  return str.replace(/['"]/g, '');
}